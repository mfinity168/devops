#!/bin/bash
# ------------------------------
# MySQL v8.0.28
# - Docker image: mysql:8.0.28
# ------------------------------
# Created: 25.07.2021
# Updated: 14.02.2022
# ------------------------------

COPY_FROM_PATH=""
COPY_TO_PATH=""
IMAGE_NAME="mysql:8.0.28"

docker pull $IMAGE_NAME

cp -arv $COPY_FROM_PATH $COPY_TO_PATH

clear
echo ""
echo "MySQL installed"
