#!/bin/bash
# ------------------------------
# PostgreSQL v14.7
# - Docker image: bitnami/postgresql:14.7.0-debian-11-r20
# ------------------------------
# Created: 25.07.2021
# Updated: 13.04.2023
# ------------------------------

COPY_FROM_PATH=""
COPY_TO_PATH=""
IMAGE_NAME="bitnami/postgresql:14.7.0-debian-11-r20"

docker pull $IMAGE_NAME

cp -arv $COPY_FROM_PATH $COPY_TO_PATH

clear
echo ""
echo "PostgreSQL installed"
