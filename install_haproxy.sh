#!/bin/bash
# ------------------------------
# SSL, Reverse Proxy
# Install Packages
# - HAProxy v2.4.17
# - Certbot v0.40.0
# ------------------------------
# Created: 17.01.2021
# Updated: 10.07.2022
# ------------------------------

timezone="Asia/Bangkok"

# ------------------------------
# Add repository: haproxy
# ------------------------------
add-apt-repository ppa:vbernat/haproxy-2.4 -y
apt update
apt install -y p7zip-full apt-transport-https ca-certificates curl software-properties-common

# ------------------------------
# Ubuntu Server Setup Timezone 
# ------------------------------
ln -fs /usr/share/zoneinfo/$timezone /etc/localtime
dpkg-reconfigure -f noninteractive tzdata

# ------------------------------
# Install Packages: haproxy, certbot
# ------------------------------
apt install haproxy -y
apt install certbot -y

clear
echo ""
echo "Packages installed"
echo "Rebooting..."
sleep 3
reboot
