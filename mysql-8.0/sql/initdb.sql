use mysql;
create user 'test_replicator'@'%' identified by 'pass';
grant replication slave on *.* to 'test_replicator'@'%';
FLUSH PRIVILEGES;
SHOW MASTER STATUS;
SHOW VARIABLES LIKE 'server_id';