#!/bin/bash
# ------------------------------
# High Availability (HA)
# SSL, Reverse Proxy
# Install Packages
# - HAProxy v2.4.17
# - Ansible v2.12.1
# - Certbot v0.40.0
# - KeepAlived v2.0.19
# ------------------------------
# Created: 17.01.2021
# Updated: 10.07.2022
# ------------------------------

timezone="Asia/Bangkok"

# ------------------------------
# Add repository: haproxy, ansible
# ------------------------------
add-apt-repository ppa:vbernat/haproxy-2.4 -y
apt-add-repository ppa:ansible/ansible -y

apt update
apt install -y p7zip-full apt-transport-https ca-certificates curl software-properties-common

# ------------------------------
# Setup Timezone 
# ------------------------------
ln -fs /usr/share/zoneinfo/$timezone /etc/localtime
dpkg-reconfigure -f noninteractive tzdata

# ------------------------------
# Install Packages
# ------------------------------
apt install haproxy -y
apt install certbot -y
apt install ansible -y
apt install keepalived -y

clear
echo ""
echo "Packages installed"
echo "Rebooting..."
sleep 3
reboot
