#!/bin/
# ------------------------------
# Install Packages
# - Docker version 23.0.3, build 3e7cbfd
# - docker-compose v1.29.2, build 5becea4
# ------------------------------
# Created: 17.01.2021
# Updated: 13.04.2023
# ------------------------------

timezone="Asia/Bangkok"
docker_compose="1.29.2"

apt update
apt install -y apt-transport-https ca-certificates curl software-properties-common

# ------------------------------
# Ubuntu Server Setup Timezone 
# ------------------------------
ln -fs /usr/share/zoneinfo/$timezone /etc/localtime
dpkg-reconfigure -f noninteractive tzdata

# ------------------------------
# Install Docker
# ------------------------------
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-get install docker-ce docker-ce-cli containerd.io -y

# ------------------------------  
# Install Docker Compose
# ------------------------------
curl -L --fail https://github.com/docker/compose/releases/download/$docker_compose/run.sh -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

clear
echo ""
echo "Packages installed"
echo "Rebooting..."
sleep 3
reboot
