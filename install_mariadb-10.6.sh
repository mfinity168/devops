#!/bin/bash
# ------------------------------
# MariaDB v10.6.7
# - Docker image: mariadb:10.6.7
# ------------------------------
# Created: 24.02.2022
# Updated: 27.02.2022
# ------------------------------

COPY_FROM_PATH=""
COPY_TO_PATH=""
IMAGE_NAME="mariadb:10.6.7"

docker pull $IMAGE_NAME

cp -arv $COPY_FROM_PATH $COPY_TO_PATH

clear
echo ""
echo "MariaDB installed"
