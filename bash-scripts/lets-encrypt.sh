#!/bin/bash
# ------------------------------
# Let's Encrypt (Certbot)
# ------------------------------
# Created: 14.01.2022
# Update: 30.04.2022
# ------------------------------

DOMAIN_FILE="./lets-encrypt-domain.txt"
CERTBOT_PATH="/etc/letsencrypt/live"
HAPROXY_SSL_PATH="/etc/haproxy/certs"

### Read domain from file
i=1
while read GET_DOMAIN
do
  ### Skip comment[#]
  if [[ "$GET_DOMAIN" != *#* ]]; then
    DOMAIN[$i]=$GET_DOMAIN
    i=$i+1
  fi
### Get File
done < $DOMAIN_FILE

clear
TITLE_MENU="Select Menu[1-3]: "
RETURN_MENU=("Create SSL" "Renew SSL" "Quit")
MENU=("Install Let's Encrypt" "Create SSL" "Renew SSL" "Quit")
PS3=$TITLE_MENU

echo "-----------------"
echo "| Let's Encrypt |"
echo "| Select Menu   |"
echo "-----------------"

select choose_menu in "${RETURN_MENU[@]}"
do

  case $choose_menu in

# --------------------
# Create SSL
# --------------------
  "Create SSL")

  clear
  echo -n "Domain name http[s]://"
  read domain_name

  ### Command ###
  service haproxy stop
  certbot certonly --standalone -d $domain_name
  sudo -E bash -c 'cat '$CERTBOT_PATH'/'$domain_name'/fullchain.pem '$CERTBOT_PATH'/'$domain_name'/privkey.pem > '$HAPROXY_SSL_PATH'/'$domain_name'.pem'
  service haproxy start

  ;;

# --------------------
# Renew SSL
# --------------------
  "Renew SSL")

  service haproxy stop
  sleep 1

  letsencrypt renew
  sleep 1

  for (( i=1; i<=${#DOMAIN[@]}; i++ )) ; do
    echo "Set PEM Files "${DOMAIN[i]}
    sudo -E bash -c 'cat '$CERTBOT_PATH'/'${DOMAIN[i]}'/fullchain.pem '$CERTBOT_PATH'/'${DOMAIN[i]}'/privkey.pem > '$HAPROXY_SSL_PATH'/'${DOMAIN[i]}'.pem'
  sleep 1
  done

  sleep 1
  service haproxy start
  
  echo "Renew Success"

  ;;

# --------------------
# Quit
# --------------------
  "Quit")
  clear
  break
  ;;

  *)
  esac
done
