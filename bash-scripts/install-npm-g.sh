#!/bin/bash
# NPM Install Global Packages
# Created: 14.09.2021
# Update: 10.07.2022

npm i -g npm-check-updates@15.2.6

### Front-end
npm i -g @quasar/cli@1.3.2

### Back-end
npm i -g nodemon@2.0.19
