#!/bin/bash
# Install packages for development on macOS
# Created: 28.10.2021
# Update: 29.04.2022

### Install Homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
# brew install wget

### Install NVM
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
