#!/bin/bash
# Build Docker Image
# Created: 14.09.2021
# Update: 29.04.2022

NAME="IMAGE_NAME"

clear
echo ""
echo -n "TAG Image (Ex. 2021.08.08-build-1) : "
read TAG

clear
echo ""
echo "TAG Image : "$NAME:$TAG
echo ""

docker build -t $NAME:$TAG ../ --no-cache
