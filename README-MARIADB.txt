###################################
#   MariaDB v10.6.7
#   24.02.2022
###################################

# Create User

# New User localhost
create user '[USERNAME]'@'localhost' identified by '[PASSWORD]';

# New User All Host
create user '[USERNAME]'@'%' identified by '[PASSWORD]';

grant all privileges on *.* TO '[USERNAME]'@'%' identified by '[PASSWORD]';
grant all privileges on [DATABASE_NAME].* TO '[USERNAME]'@'%' identified by '[PASSWORD]';

flush privileges;

# install mariadb
apt update
apt install mariadb-server -y

# First node
mysql_secure_installation

vi /etc/mysql/conf.d/galera.cnf

systemctl stop mariadb

# First node
galera_new_cluster

# Second node
systemctl start mariadb

mysql -u root -p

# Test
mysql -u root -p -e "create database db2"
mysql -u root -p -e "show databases"

mysql -u root -p -e "show status like 'wsrep_cluster_size'"
mysql -u root -p -e "show status like 'wsrep_cluster_status'"
mysql -u root -p -e "show status like 'wsrep_incoming_addresses'"

###################################
#   Recover
#   https://www.symmcom.com/docs/how-tos/databases/how-to-recover-mariadb-galera-cluster-after-partial-or-full-crash
###################################

rm /var/lib/mysql/grastate.dat
rm /var/lib/mysql/ib_logfile*
galera_new_cluster

systemctl start mariadb
