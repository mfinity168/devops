#!/bin/bash
# Backup postgresql 14.2
# Created: 08.01.2022
# Update: 29.04.2022

# * * * * *  NOTE * * * * *
# Config:
#   - git config user.email /user.username
#   - config source config file
#   - Folder
#     .git/config > [remote "origin"] > url
#   - postgresql.config.sh

# =========================================
#   Use config from file
# =========================================
source "/PATH/postgresql.config.sh"

### Read db name from file
i=1
while read get_db
do
  ### Skip comment[#]
  if [[ "$get_db" != *#* ]]; then
    db[$i]=$get_db
    i=$i+1
  fi
### Get File
done < $db_file

# =========================================
#   BACKUP All Database
# =========================================
mkdir -p $backup_path
start_time=$(date +"%H:%M:%S")

  for (( i=1; i<=${#db[@]}; i++ )) ; do
    docker exec $container_name sh -c "exec pg_dump -U $user_db ${db[i]} --no-password --format=c --blobs" | \
    7z -p$pass_encrypt a -t7z -v$limit_compress -si${db[i]} $backup_path/${db[i]}$file_name.7z
    end_time=$(date +"%H:%M:%S")
  done

  echo "[Backup Time: "$start_time" - "$end_time"]" >> $backup_path/"_backup_time.txt"

sleep 1

# =========================================
#   Git push to backup Server
# =========================================
cd $path
git add .
git commit -m 'Backup Date: '$backup_date
git push origin $git_branch

