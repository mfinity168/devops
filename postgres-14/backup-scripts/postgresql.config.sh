#!/bin/bash
# PostgreSQL 14.1
# Config Files
# Created: 08.01.2022
# Update: 15.01.2022

# =========================================
#   Host Config
# =========================================
db_file="/PATH/database.txt"
backup_date=$(date +"%Y.%m.%d_%H.%M")
file_name="_$(date +"%Y.%m.%d_%H.%M")"
path=""
container_name=""
git_branch="main"
backup_path="$path/$container_name"/$(date +"%Y").$(date +"%m").$(date +"%d")

# =========================================
#   Host Database Config
# =========================================
user_db=""
pass_db=""
pass_encrypt=""
limit_compress="8m"
