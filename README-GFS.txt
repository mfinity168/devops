###################################
#   GlusterFS v10.1
#   29.03.2022
###################################

# ------------------------------
#   Server
# ------------------------------
vi /etc/hosts

192.168.27.176 gfs-1
192.168.27.177 gfs-2
192.168.27.178 gfs-3

add-apt-repository ppa:gluster/glusterfs-10
apt update
apt install -y glusterfs-server

systemctl start glusterd.service
systemctl enable glusterd.service
systemctl status glusterd.service

# ------------------------------
#   dispersed volume
# ------------------------------
gluster peer probe gfs-2
gluster peer probe gfs-3

gluster peer status

# Create a dispersed volume:
gluster volume create kyl-storage disperse 3 redundancy 1 \
gfs-1:/mnt/kyl-storage \
gfs-2:/mnt/kyl-storage \
gfs-3:/mnt/kyl-storage force

gluster volume start kyl-storage
gluster volume status
gluster volume info

# Client
mount -t glusterfs gfs-1:/kyl-storage /opt

# Test Creating 10 files of 20M (count = 10)
for i in `seq 1 10`; do dd if=/dev/urandom of=/opt/test-a_$i.img bs=2M count=10; done

# Test Creating 5 files of 100M (count = 10)
for i in `seq 1 5`; do dd if=/dev/urandom of=/opt/test-b_$i.img bs=10M count=10; done

# Test Creating 2 file of 500M (count = 10)
for i in `seq 1 2`; do dd if=/dev/urandom of=/opt/test-c_$i.img bs=1M count=500; done

# Test Creating 1 file of 1G (count = 1000)
for i in `seq 1 1`; do dd if=/dev/urandom of=/opt/test-d_$i.img bs=1M count=1000; done

# ------------------------------
#   Replicated volume
# ------------------------------
gluster peer probe gfs-2

gluster peer status

gluster volume create kyl-volume replica 2 transport tcp \
gfs-1:/mnt/kyl-volume \
gfs-2:/mnt/kyl-volume force

gluster volume start kyl-volume
gluster volume status
gluster volume info

# ------------------------------
#   Allow IP
# ------------------------------
gluster volume set [volume] auth.allow 10.0.2.15,192.168.27.44
OR
gluster volume set [volume] auth.allow 192.168.27.*,10.0.2.15

gluster volume get [volume] auth.ssl-allow

# ------------------------------
#   quota
# ------------------------------
# enable/disable
gluster volume quota test-volume [enable/disable]

# limit-usage
# gluster volume quota [volume] limit-usage [/path] [limit]
gluster volume quota test-volume limit-usage /data 10GB

# ------------------------------
#   Remove brick (node)
# ------------------------------
# Step 1 remove-brick: replica 2 = Reduce 3 node to 2 node
gluster volume remove-brick kyl-volume replica 2 gfs-2:/mnt/kyl-volume force

# Step 2: detach node
gluster peer detach gfs-2

# ------------------------------
#   Add new brick (node)
# ------------------------------
# Step 1: peer node
gluster peer probe gfs-2
gluster peer status

# Step 2 add-brick: replica 3 = Incress 2 node to 3 node
gluster volume add-brick kyl-volume replica 3 gfs-2:/mnt/kyl-volume force

# ------------------------------
#   Replica 3 nodes
#   Failover 2 nodes (node2,node3)
# ------------------------------

# Step 1 remove-brick: replica 2 = Reduce 3 node to 2 node
gluster volume remove-brick kyl-volume replica 2 gfs-3:/mnt/kyl-volume force

# Step 2: detach node
gluster peer detach gfs-3 force

# Step 3 remove-brick: replica 1 = Reduce 2 node to 1 node
gluster volume remove-brick kyl-volume replica 1 gfs-2:/mnt/kyl-volume force

# Step 4: detach node
gluster peer detach gfs-2 force

# Step 5: peer node
gluster peer probe gfs-2
gluster peer status

# Step 6 add-brick: replica 2 = Incress 1 node to 2 node
gluster volume add-brick kyl-volume replica 2 gfs-2:/mnt/kyl-volume force

# ------------------------------
#   Snapshot
# ------------------------------

# ------------------------------
#   Client
# ------------------------------
add-apt-repository ppa:gluster/glusterfs-10
apt update
apt install -y glusterfs-client

reboot

mkdir -p /mnt/kyl-volume
mount -t glusterfs gfs-1:/kyl-volume /mnt/kyl-volume

# Automatically Mounting Volumes
vi /etc/fstab

HOSTNAME-OR-IPADDRESS:/VOLNAME MOUNTDIR glusterfs defaults,_netdev 0 0

# For example:
gfs-1:/kyl-volume /opt/kyl-volume-01 glusterfs defaults,_netdev 0 0
OR
gfs-1:/kyl-volume /opt/kyl-volume glusterfs defaults,_netdev,log-level=WARNING,log-file=/var/log/gluster.log 0 0

