###################################
#   MySQL v8.0.28
#   Replication Master-Master
#   29.03.2022
###################################

###################################
#   MySQL Cluster
###################################

docker network create cluster --subnet=192.168.0.0/16

docker run -d --net=cluster --name=management1 --ip=192.168.0.2 mysql/mysql-cluster:8.0.28 ndb_mgmd

docker run -d --net=cluster --name=ndb1 --ip=192.168.0.3 mysql/mysql-cluster:8.0.28 ndbd
docker run -d --net=cluster --name=ndb2 --ip=192.168.0.4 mysql/mysql-cluster:8.0.28 ndbd

docker run -d --net=cluster --name=mysql1 -p 3306:3306 --ip=192.168.0.10 -e MYSQL_RANDOM_ROOT_PASSWORD=true -e MYSQL_ROOT_HOST='%' mysql/mysql-cluster:8.0.28 mysqld

docker logs mysql1 2>&1 | grep PASSWORD
docker exec -it mysql1 mysql -uroot -p
ALTER USER 'root'@'localhost' IDENTIFIED BY 'MyNewPass';

CREATE USER 'remote'@'%' IDENTIFIED BY 'p@ssw0rd';
ALTER USER 'remote'@'%' IDENTIFIED WITH mysql_native_password BY 'p@ssw0rd';

SELECT user, host, account_locked, password_expired FROM mysql.user;

docker run -it --net=cluster mysql/mysql-cluster:8.0.28 ndb_mgm

###################################
#   Setup Master1/Master2
#   Master-Master
###################################
# STEP 1 - Master1
mysql -u root -p

create user 'replicator'@'%' identified by 'pass';
grant replication slave on *.* to 'replicator'@'%';

show master status;
# mysql-bin.000003 671

# STEP 2 - Master2
mysql -u root -p

create user 'replicator'@'%' identified by 'pass';
grant replication slave on *.* to 'replicator'@'%';

# STEP 3 - Master2 to Master1
stop slave;
CHANGE MASTER TO MASTER_HOST = '192.168.0.2', MASTER_USER = 'replicator', MASTER_PASSWORD = 'pass', MASTER_LOG_FILE = 'mysql-bin.000003', MASTER_LOG_POS = 671;
start slave;

show master status;
# mysql-bin.000003 671

# STEP 4 - Master1 to Master2
stop slave;
CHANGE MASTER TO MASTER_HOST = '192.168.0.3', MASTER_USER = 'replicator', MASTER_PASSWORD = 'pass', MASTER_LOG_FILE = 'mysql-bin.000003', MASTER_LOG_POS = 671;
start slave;

show master status;
# mysql-bin.000003 671

# STEP 5 - Master1
create database db_01;
create database db_02;

# STEP 6 - Master2
show databases;

###################################
#   Recovery Master1/Master2
###################################


###################################
#   New database replication
###################################
# STEP 1 -  Master1 Stop/start MySQL (Docker)
docker-compose down && docker-compose up -d

# STEP 2 - Master1
# Reset slave/master
mysql -u root -p

stop slave;
reset slave;
reset master;
start slave;

show master status;
# mysql-bin.000001 156

# STEP 3 - Master2 to Master1
mysql -u root -p

stop slave;
reset slave;
reset master;
CHANGE MASTER TO MASTER_HOST = '192.168.0.2', MASTER_USER = 'replicator', MASTER_PASSWORD = 'pass', MASTER_LOG_FILE = 'mysql-bin.000001', MASTER_LOG_POS = 156;
start slave;

show master status;
# mysql-bin.000001 156

# STEP 4 - Master1 to Master2
stop slave;
CHANGE MASTER TO MASTER_HOST = '192.168.0.3', MASTER_USER = 'replicator', MASTER_PASSWORD = 'pass', MASTER_LOG_FILE = 'mysql-bin.000001', MASTER_LOG_POS = 156;
start slave;

# STEP 5
show databases;
