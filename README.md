# DevOps install Script

# For Ubuntu Server 20.04 or lastest

Developer: mfinity168  
Created: 20.01.2021  
Updated: 13.04.2023

# Packages

- HAProxy v2.4.17
- Ansible v2.12.2
- KeepAlived v2.0.19
- Nginx v1.20.2
- Certbot v0.40.0
- Docker version 23.0.3, build 3e7cbfd
- docker-compose v1.29.2, build 5becea4c
- PostgreSQL v14.7
- MariaDB v10.6.7
- MySQL v8.0.28
- GlusterFS v10.1

# Ansible

- HAProxy config file
- KeepAlived config file

# server-scripts

- lets-encrypt.sh (Create/Renew)

# install_haproxy_ha.sh

- HAProxy
- Ansible
- Certbot
- KeepAlived

# install_nginx_ha.sh

- Nginx
- Ansible
- Certbot
- KeepAlived

# install_haproxy.sh

- HAProxy
- Certbot

# install_docker.sh

- Docker
- Docker Compose

# install_postgres-14.2.sh

- postgres docker image
- docker-compose.yml
- PostgreSQL config file

# install_mariadb-10.6.sh

- MariaDB docker image
- docker-compose.yml
- MariaDB config file

# install_mysql-8.0.sh

- mysql docker image
- docker-compose.yml
- MySQL config file
