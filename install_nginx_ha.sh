#!/bin/bash
# ------------------------------
# High Availability (HA)
# SSL, Reverse Proxy
# Install Packages
# - Nginx v1.20.2
# - Ansible v2.12.2
# - Certbot v0.40.0
# - KeepAlived v2.0.19
# ------------------------------
# Created: 08.01.2022
# Updated: 29.03.2022
# ------------------------------

timezone="Asia/Bangkok"

# ------------------------------
# Add repository: nginx, ansible
# ------------------------------
curl https://nginx.org/keys/nginx_signing.key | gpg --dearmor | sudo tee /usr/share/keyrings/nginx-archive-keyring.gpg >/dev/null
gpg --dry-run --quiet --import --import-options import-show /usr/share/keyrings/nginx-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/nginx-archive-keyring.gpg] \
http://nginx.org/packages/ubuntu `lsb_release -cs` nginx" \
  | sudo tee /etc/apt/sources.list.d/nginx.list

apt-add-repository ppa:ansible/ansible -y

apt update
apt install -y p7zip-full apt-transport-https ca-certificates curl software-properties-common

# ------------------------------
# Ubuntu Server Setup Timezone 
# ------------------------------
ln -fs /usr/share/zoneinfo/$timezone /etc/localtime
dpkg-reconfigure -f noninteractive tzdata

# ------------------------------
# Install Packages:
#   nginx, certbot, ansible
# ------------------------------
apt install nginx -y
apt install certbot -y
apt install ansible -y

clear
echo ""
echo "Packages installed"
echo "Rebooting..."
sleep 3
reboot
