###################################
#   HAProxy v2.4.15
#   29.03.2022
###################################

# ------------------------------
#   Set up sv-ha-01
# ------------------------------

- Add ssh-key to gitlab.psu.ac.th

- Add ssh-key to server-ha-02

- Edit hostname
$ vi /etc/hostname

- Add host alias
$ vi /etc/hosts

IP_1 SERVER-NAME-01
IP_2 SERVER-NAME-02

- Clone Script
$ git clone https://gitlab.com/mfinity168/devops.git

- Install Packages
$ bash install_haproxy_ha.sh

- Edit, Create Let's Encrypt
$ bash server-scripts/lets-encrypt.sh

- Clone config
$ git clone git@gitlab.psu.ac.th:kyl-config/ha.git

- edit, copy config to haproxy, keepalived with ansible
$ ansible-playbok haproxy_ha/setup.yml

# ------------------------------
#   Install Packages on sv-ha-02
# ------------------------------

- [server-ha-01] Install Packages, copy config file with ansible
$ ansible-playbok haproxy_ha/install.yml
